(function () {
    "use strict";
    /*global $, jQuery, console */
    var latitude, longitude, url, Location, uur, mylocation, day, month, icoon, dag,  daynumber, monthnumber, year, date;
    
    Location = function () {
        
        var dateTomorrow = new Date();
        
        dateTomorrow.setDate(dateTomorrow.getDate() + 1);
        console.log(dateTomorrow.getDate());
        
        day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        month = ["January", "February", "March", "April", "May", "Juny", "July", "August", "September", "October", "November", "December"];
        daynumber = dateTomorrow.getDay();
        date = dateTomorrow.getDate();
        monthnumber = dateTomorrow.getMonth();
        year = dateTomorrow.getFullYear();
        $("#dateTomorrow").html(date + " " + month[monthnumber] + " " + year);
        $("#day").html(day[daynumber]);
        
        
        this.getCurrentLocation = function () {
            if (navigator.geolocation) {
                console.log("geolocation werkt");
                navigator.geolocation.getCurrentPosition(this.showLocation);
            } else {
                console.log("Geolocation is not supported by this browser.");
            }
        };
        
        this.showLocation = function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            
            console.log(latitude);
            console.log(longitude);
            
            url = "https://api.forecast.io/forecast/e211002ce64448f481641f0ca287d1af/" + latitude + "," + longitude + "?units=si";
            
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                url: url,
                success: function (resp) {
                    uur = 24;
                    dag = 1;
                    console.log(resp);
                    $("#degrees").html(Math.round(resp.hourly.data[uur].temperature) + " °C");
                    $("#min").html("min " + Math.round(resp.daily.data[dag].temperatureMin) + " °C");
                    $("#max").html("max " + Math.round(resp.daily.data[dag].temperatureMax) + " °C");
                    $("#sum").html(resp.daily.data[dag].summary);
                    icoon = "picture/" + resp.daily.data[dag].icon + ".png";
                    console.log(icoon);
                    $("#picture").attr("src", icoon);

                    //localstorage
                    
                    localStorage.setItem("localData", JSON.stringify(resp));
                    if (localStorage.getItem("temperatur") !== null) {
                        temperature = JSON.parse(localStorage.getItem("temperature"));
                    }
                }
            });
        };
    };
    
    mylocation = new Location();
    mylocation.getCurrentLocation();
}());