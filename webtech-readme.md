## Webtech 2 - opdrachten ##


Hallo, mijn naam is Jonas Reymen en zal u eventjes alles uitleggen wat we de voorbije weken tijdens het vak webtech gedaan hebben. In onderstaande weken leg ik even uit wat ik gedaan heb bij het maken van de opdrachten.

#### Week 1: GIT ####

GIT is een tool die gebruikt word tussen groepen, het is zeer handig om zo code door te sturen en de andere teamleden weten ook perfect wat je gedaan hebt. Ook kan je dit gebruiken voor werk dat jezelf maakt, je kan alle files bij elkaar zetten. Hieronder heb ik een paar commands beschreven die zeer handig zijn bij het gebruiken van GIT

	git init

"git init" zorgt ervoor dat je een repository aanmaakt in het mapje waar je je bevindt. Je kan dit doen op een leeg mapje maar ook op een bestaand mapje waar al wat code of files inzitten. Zonder repository kan je ook helemaal niets doen en daarom moet je altijd beginnen met deze command.
	
	git clone https://jonasr001@bitbucket.org/jonasr001/webtech_2.git

git clone zorgt ervoor dat je een repository kunt clonen, zoals de naam al zegt worden de bestanden van de repository gekopieerd in het mapje waar je git clone hebt gedaan. Bovenstaand voorbeeld zorgt er bijvoorbeel voor dat je mijn bitbucket repository kopieerd.

	git add *

git add zorgt ervoor dat je changes of aanpassingen binnen je files kan adden, en daarna bijvoorbeel kunt doorsturen naar je bitbucket repository. Bovenstaand voorbeeld zorgt ervoor dat je alles kan adden wat je aangepast hebt, maar je kan natuurlijk ook gewoon de changes binnen een bestand adden door git add file.name .

	git commit -m "voorbeeld aangepast"

git commit zorgt ervoor dat de nieuwe changes gecommit worden, bij commit kan je dan een tekstje meegeven dat er changes hebben voor gedaan en dan kunnen de andere teamleden zien wat je nu juist veranderd hebt. Bovenstand voorbeeld zorgt er voor dat je een change gecommit hebt waaraan je een tekstje gekoppeld hebt, namelijk dat je het voorbeeld hebt aangepast.

	git rm filename

git rm laat zien dat je een bestand verwijderd en dat je deze moet verwijderen binnen je repository, dit doe je via git rm. Bovenstaand voorbeeld zorgt ervoor dat je een bestand verwijderd met naam "filename".

	git status

git status zorgt ervoor dat je de status kan checken van de repository, je kan bijvoorbeeld zien welke files nog "untracked", welke files aangepast zijn, welke files verwijderd zijn enz.

	git push origin master

git push zorgt ervoor dat je de files uit je lokaal repository kan pushe naar de bitbucketrepository. Bovenstaand voorbeeld zorgt ervoor dat je de files naar je remote repository doorstuurt.

	git branch mijnbranch

git branch zorgt ervoor dat je een nieuwe tak aanmaakt zodat je niet altijd moet werken via de master branch of tak. Dus wanneer er een fout in je code zit, dan zit deze niet direct in de master maar in de branch waarin je zit te werken. Bovenstaand voorbeeld zorgt ervoor dat je een branch aanmaakt met naam "mijnbranch".

	git merge 

git merge zorgt ervoor dat je 1 of meerdere branches kunt samenvoegen in de branch waar jij zit in te werken.

	git checkout

Hierdoor kan je van branch wisselen.

#### Week 2: CSS3 ####
	
**Opdracht 1:**
	
	Een animatie waarbij je een soort navigatie nabouwt door middel van een knop,
	nadat er op de knop wordt gedrukt verschijnen er 3 bolletjes die de navigatie 
	van de website voorstelt. Na het drukken op het hetzelfde knopje gaan deze
	3 bolletjes terug naar hun normale positie.

Bij deze opdracht heb ik met een transition gewerkt omdat ik deze kan laten werken wanneer er op de knop gedrukt word. 

Ik heb gewerkt met div's omdat het gemakkelijk op te maken is, ik heb zo deze div's rond gemaakt via een borderradius, daarna heb ik de 3 aparte div's een id gegeven die ik daarna een gepaste kleur heb gegeven via css. Nu voor de animatie, Ik heb via een onclick methode de bolletjes allemaal tegelijk laten bewegen. Ik heb deze onclick enkel op de gele bol gezet zodat wanneer je op deze bol drukt dat je alle bolletjes ziet bewegen en wanneer je terug op de gele bol drukt dan gaan de bolletjes terug naar de oorspronkelijke positie. 

	function positionering() {
			document.getElementById("yellow").setAttribute("class", "movedown");
			document.getElementById("yellow").setAttribute("onclick", "terug()");
			document.getElementById("red").setAttribute("class", "moveright");
			document.getElementById("green").setAttribute("class", "moverdown");
	}
	
	function terug(){
			document.getElementById("yellow").setAttribute("class", "backdown");
			document.getElementById("yellow").setAttribute("onclick", "positionering()");
			document.getElementById("red").setAttribute("class", "backright");
			document.getElementById("green").setAttribute("class", "backrdown");
	}

Dus bij een klik geef ik de bolletjes een andere class die ervoor zorgt dat de bolletjes beginnen te bewegen. Ook zorg ik ervoor dat als je terug op de gele bol drukt dat er een andere functie wordt in gang gezet namelijk de functie terug(); .

	.movedown{
			transform: translateX(50px);
			transition: all 1s ease-in;
	}	

Zoals ik al zei, heb ik gewerkt met een transition die ervoor zorgt dat de bolletjes binnen de 1 seconden op de andere plaats zijn, via translateX en translateY heb ik ervoor gezorgd dat de bolletjes naar onder, boven of schuin bewegen.
	
**Opdracht 2:**
	
	Een animatie waarbij er een soort van lijst van images komt binnen gevlogen
	ook de tekst en de omkadering van de tekst heeft een speciaal effectje.
____

	-webkit-animation: flyin 1s;
	transform-origin: left;
	
	@-webkit-keyframes rotation{
			0%{
				transform: perspective(800px) rotateX(90deg);
			}
	}

Via deze oefening heb ik leren werken met animations die ervoor zorgt dat wanneer de website geladen word, dat er een animatie meot afspelen bij deze oefening zitten 2 animaties, 1 voor de rotatie en 1 voor het invliegen van de tekst. Hierbij heb ik ook moeten werken met 3D via de perspective, perspective zorgt ervoor dat de hoek van het kijken wordt aangepast. Dus om te beginnen hebben we de foto's die moeten binnengedraait worden. Dit via een rotateY die ik zo goed mogelijk hebben moeten nabouwen volgens het voorbeeld, hierbij heb ik een speed gegeven of een snelheid van 1 second.
Tegelijkertijd doet er zich ook nog een andere CSS functie voor namelijk de rotation functie die ervoor zorgt dat de de kader vanboven draait op een horizontalelijn dit via een rotateX. Ook laat ik de tekst binnenvliegen via de functie comein();, dit door translateX.

**Opdracht 3:**
	
	Opdracht 3 gaat over het maken van een animatie waarbij je een soort todolist
	maakt, met een druk op de knop krijg je een nieuw listitem, bij het drukken
	op een listitem, verdwijnt deze met een speciaal effectje.
____

	ul#todo li
	{
		transition: all 0.2s ease-in;
	    -webkit-transform: perspective(300px)rotatex(45deg); 
	}

Via de startbestanden heb ik gemerkt dat wanneer je op een todoitem drukt dat de class wordt aangepast naar niks, dus de classname valt weg. Daarom is het vrij gemakkelijk om ervoor te zorgen dat dit todoitem een transition te geven (na klik), geef ik de ul#todo li een transition dat na de klik word uitgevoerd, namelijk dat via perspective het todoitem uitdraait. ook wanneer een todoitem word aangemaakt kan ik dit gemakkelijk laten indraaien namelijk via dezelfde transition.  

**Opdracht 4:**

	En ten slotte de animatie waarbij je eerst een figuurtje ziet passeren. na,
	het verdwijnen krijg komen er weer 3 andere figuurtjes in.

Bij de 4de opdracht heb ik enkel animations gebruikt omdat de animatie moet werken wanneer de pagina geladen word. ik heb 2 keyframes gemaakt, 1 voor de kleur te laten veranderen en de andere voor de figuurtjes te laten verschijnen en deze naar boven te laten bewegen, het naar boven laten bewegen heb ik via translateY gedaan. nu het moeilijke bijd e figuurtjes is vooral het op de juiste moment te laten binnengooien. Dit heb ik gedaan via een class die ik first noem en de andere een andere class name te geven. de first begint zoals gewoonlijk op 0 seconden die 3 seconden duurt terwijl de 3 andere op een ander tijdstip binnenkomen dit via een delay die ik op 3.25 seconden heb gezet (op de 1ste 2 figuurtjes) de laatste figuur heb ik een delay van 3.3 genomen omdat deze later uit en in het scherm moet komen.


### Week 3: Advanced javascript ###

	Deze week hebben we wat meer geleerd over Javascript. Voor deze week 
	hebben we een opdracht gekregen die te maken had met het  maken van een 
	todolist waarbij we via een framework de todoitems konden laten veranderen.
	Zo hebben we leren werken met een framework

### Week 4: API ###

Deze week hebben we leren werken met een API, een API is een soort verzameling van data gestockeerd in een soort bibliotheek. Na het aanspreken van een API kan je dan de data uit de bibliotheek halen bijvoorbeeld voor het weer weer te geven.

	this.getCurrentLocation = function () {
            if (navigator.geolocation) {
                console.log("geolocation werkt");
                navigator.geolocation.getCurrentPosition(this.showLocation);
            } else {
                console.log("Geolocation is not supported by this browser.");
            }
    };

	this.showLocation = function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
	};

We hebben de locatie waar u zich bevindt bepaalt door gebruik te maken van geolocation, geolocation zorgt ervoor eigenlijk voor dat je de x en y waarden van uw plaats opvangt zodat u deze kan gebruiken voor het weer aan te roepen bijvoorbeeld. Wij hebben de API aangeroepen via een ajax call (onderstaand voorbeeld) dit wil zeggen dat je een url meegeeft aan deze call zodat deze kan connecteren met de API de longitude en latitude heb ik via de geolocation vastgelegt. Na het aanroepen van de API kan je dan de data aanroepen en neerschrijven in je applicatie. Deze data staat in een soort van array die je op een vrij makkelijke manier kon aanroepen.
Bij deze oefening hebben we ook localstorage gebruikt, localstorage zorgt ervoor dat je data locaal op eimand zijn browser kan storagen. In het voorbeeld van de weerapp hebben we dit gedaan via de code die door localstorage word gebruikt namelijk localstorage.setitem en localstorage.getitem.

	$.ajax({
                type: "GET",
                dataType: "jsonp",
                url: url,
                success: function (resp) {
                    uur = 24;
                    dag = 1;
                    console.log(resp);
                    $("#degrees").html(Math.round(resp.hourly.data[uur].temperature) + " °C");
                    $("#min").html("min " + Math.round(resp.daily.data[dag].temperatureMin) + " °C");
                    $("#max").html("max " + Math.round(resp.daily.data[dag].temperatureMax) + " °C");
                    $("#sum").html(resp.daily.data[dag].summary);
                    icoon = "picture/" + resp.daily.data[dag].icon + ".png";
                    console.log(icoon);
                    $("#picture").attr("src", icoon);

                    //localstorage
                    
                    localStorage.setItem("localData", JSON.stringify(resp));
                    if (localStorage.getItem("temperatur") !== null) {
                        temperature = JSON.parse(localStorage.getItem("temperature"));
                    }
                }
   	});


### Week 5: Node.js ###

Deze week hebben we leren werken met node.js, node.js is een platform dat gebruikt word voor het ontwikkelen van applicaties. De applicaties worden geschreven in Javascript. Bij het maken van een node applicatie moeten we eerst een aantal zaken installeren zoals de modules. Men moet eerst de mongod en mongo opstarten. Daarna gaan we naar de commandline waar we een aantal commands moeten invoeren.

#### Non-blocking io ####

Non-blocking IO is een vorm van input en output verwerking. het wil eigenlijk gewoon zeggen dat de processen blijven doorgaan ongeacht de actie nu voltooid is of niet.

#### Installatie van een applicatie ####

	npm install express --save

express zorgt ervoor dat je de starting files kan installeren om aan een applicatiete beginnen, dus het beginnen met coderen. Natuurlijk is dit nog niet volledig, we moeten dit eerst op onze localhost laten draaien dit doen we door node bin/www of gewoon de filename waarin je wilt werken te activeren. Als dit niet werkt wilt dit zeggen dat je een aantal modules nog moet isntalleren, dit doen we door npm install, dit installeert alle modules die zullen gebruikt worden. Wanneer je een nieuwe module nodig hebt doen we net hetzelfde dus ook npm install met de naam van deze module. 

#### package.json ####

In de package.json bestand zit al de data om je applicatie te laten draaien.

#### Nodemon ####

Nodemon is een module of tool die ervoor zorgt dat wanneer je iets aanpast in de bestanden waarin je werkt dat deze de applciatie automatisch gaat updaten.

de installatie gaat als volgt:

	npm install -g nodemon

-g zorgt ervoor dat deze module globally is opgeslagen wat wil zeggen dat je dit voor alle applicaties kan gebruiken en dus niet --save wat wil zeggen dat deze gewoon zal installeren in de map waarin je zit te werken.

### Week 6: Angular.js ###

AngularJS is een JavaScript-framework dat geheel ontwikkeld is om tegemoet te komen aan de eisen van grote en complexe webapplicaties. In AngularJS ontwikkel je niet één grote applicatie waarin alles met alles verbonden is. In plaats daarvan maak je kleinere, gespecialiseerde modules. Deze zijn los van elkaar te ontwikkelen en te testen.Kenmerk van AngularJS-applicaties is wel dat de de applicatie in principe volledig in de browser draait. AngularJS is een client-sided framework.

#### Angular directives ####

Het framework werkt door eerst de HTML-pagina te lezen, waarin aanvullende specifieke HTML-attributen zijn opgenomen. Die attributen worden geïnterpreteerd als directives die ervoor zorgen dat Angular invoer- of uitvoercomponenten van de pagina koppelt aan een model dat wordt weergegeven door middel van standaard JavaScript-variabelen.

##### aantal directives: #####

	ng-app

declareert het root-element (het hoogste element in de structuur) van een Angular-applicatie, waaronder directives gebruikt kunnen worden om data-koppelingen te declareren en gedrag te definiëren.

	ng-model

deze directive ontwikkelt een data-koppeling in twee richtingen tussen de view en de scope
	
	ng-controller

koppelt een JavaScript-controller-klasse aan de view. Dit is een cruciaal onderdeel van de manier waarop Angular het model-view-controller ontwerppatroon implementeert. Controller-klassen bevatten de bedrijfslogica achter de applicatie, zij voorzien de scope van functies en waarden.

	ng-repeat

instantieert een element eenmaal per item uit een verzameling.

	ng-view

de directive die verantwoordelijk is voor het behandelen van routes die worden aangegeven door URL's. Het definiëren van routes zorgt ervoor dat templates worden weergegeven, die worden aangestuurd door gespecificeerde controllers.

	ng-class

maakt het mogelijk CSS-klasse-attributen dynamisch te koppelen aan een HTML-element. Dit gebeurt door middel van een data-koppeling van een expressie, die de toe te voegen CSS-klassen vertegenwoordigt.

### Week 7: Sass.js ###

SASS is een extensie op CSS3 en levert uiteindelijk als output een stylesheet bestand op. SASS biedt een uitgebreide set aan functies en mogelijkheden om gestructureerd te werk te gaan en zo de website te stylen. Het is zeer gemakkelijk te gebruiken en kan ons ook veel tijd besparen. Het zit ook zeer logisch in mekaar. Ook zorgt het ervoor dat veel dingen die je meer als 2 keer gebruikt, in 1 keer kan geschreven worden.

