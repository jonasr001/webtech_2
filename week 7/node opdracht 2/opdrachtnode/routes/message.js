var express = require('express');
var router = express.Router();

/* GET message. */
router.get('/', function(req, res, next) {
  res.render('message/index');
});

module.exports = router;