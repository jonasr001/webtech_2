$(document).ready(function(){
	var socket = io.connect('http://localhost:3005');

	$("#post").on("click", function(){

		var name = $("#name").val();

		var question = $("#message").val();

		//doorsturen database

		var questions = {'name' : name, 'question' : question};
		console.log(questions);
		socket.emit("questions", questions);

		//data aflezen
		
		socket.on("messageaflezen", function(data){
			$("#list").prepend("<li><h1>"+data.name + "</h1><p>" + data.question +"</p></li>");
		});


		socket.on("questionsshowen", function(questions){
			for (var i = 0; i < questions.length; i++) {

				$("#list").append("<li>" + questions[i].name + questions[i].question +"</li>");
			}
		});

	});
});